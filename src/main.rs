extern crate failure;
extern crate glib;
extern crate gio;
extern crate gtk;
extern crate regex;
extern crate rumble;

use std::env::args;

use failure::{Error, ResultExt};
use gio::prelude::*;

#[macro_use]
mod macros;
mod ble;
mod ui;

fn print_error(e: &Error) {
    use std::env;

    eprintln!("ERROR: {}", e);
    for cause in e.causes().skip(1) {
        eprintln!("\tCAUSE: {}", cause);
    }
    if let Ok(bt_enable) = env::var("RUST_BACKTRACE") {
        if !bt_enable.is_empty() {
            eprintln!("\tBACKTRACE: {}", e.backtrace());
        }
    }
}

fn run() -> Result<(), Error> {
    let app =
        gtk::Application::new("com.github.antage.heartrate", gio::ApplicationFlags::empty())
            .context("GTK initialization failed.")?;
    app.connect_startup(move |app| {
        let main_ctrl = ui::MainController::new(&app);
        main_ctrl.borrow().show_all();
    });
    app.connect_activate(|_| {});

    app.run(&args().collect::<Vec<_>>());

    Ok(())
}

fn main() {
    if let Err(ref err) = run() {
        print_error(err);
    }
}
