use rumble::bluez::adapter::Adapter;

pub enum ContextMessage {
    StartRefreshAdapters,
    StopRefreshAdapters {
        adapters: Vec<Adapter>,
    },
}
