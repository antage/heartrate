use std::marker::PhantomData;
use gio;
use gtk;
use gtk::prelude::*;

const BUILDER_DATA_APP_WINDOW: &str = include_str!("../../assets/app-window.glade");
thread_local! {
    static BUILDER: gtk::Builder = {
        let builder = gtk::Builder::new();
        builder.add_from_string(BUILDER_DATA_APP_WINDOW).unwrap();
        builder
    };
}

pub struct BuilderKey<T: gtk::IsA<gio::Object>> {
    name: &'static str,
    _marker: PhantomData<T>,
}

impl<T: gtk::IsA<gio::Object>> BuilderKey<T> {
    pub fn get(&self) -> T {
        BUILDER.with(|builder| {
            builder.get_object(self.name)
                .expect(&format!("Can't get '{}' object from the builder.", self.name))
        })
    }
}

macro_rules! builder_key {
    ($name: expr) => {
        BuilderKey {
            name: $name,
            _marker: ::std::marker::PhantomData,
        }
    }
}

pub const MAIN_WINDOW: BuilderKey<gtk::ApplicationWindow> = builder_key!("main:application_window");
pub const REFRESH_BUTTON: BuilderKey<gtk::Button> = builder_key!("refresh:button");
pub const ADAPTERS_COMBOBOX: BuilderKey<gtk::ComboBox> = builder_key!("adapters:combo_box");
pub const SCAN_BUTTON: BuilderKey<gtk::Button> = builder_key!("scan:button");
