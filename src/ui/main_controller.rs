use ble;
use gio;
use gio::prelude::*;
use glib;
use gtk;
use gtk::prelude::*;
use rumble::bluez::adapter::Adapter;
use std::cell::RefCell;
use std::rc::Rc;
use std::sync::mpsc::channel;
use std::thread;
use ui::builder;
use ui::context_message::ContextMessage;

pub struct MainController {
    self_opt_rc: Option<Rc<RefCell<MainController>>>,

    app: gtk::Application,
    window: gtk::ApplicationWindow,
    refresh_button: gtk::Button,
    adapters_store: gtk::ListStore,
    adapters_combobox: gtk::ComboBox,
    scan_button: gtk::Button,

    listener_src: Option<glib::SourceId>,

    selected_adapter: Option<Adapter>,
}

impl MainController {
    pub fn new(app: &gtk::Application) -> Rc<RefCell<MainController>> {
        let ctrl = MainController {
            self_opt_rc: None,

            app: app.clone(),
            window: builder::MAIN_WINDOW.get(),
            refresh_button: builder::REFRESH_BUTTON.get(),
            adapters_store: gtk::ListStore::new(&[String::static_type()]),
            adapters_combobox: builder::ADAPTERS_COMBOBOX.get(),
            scan_button: builder::SCAN_BUTTON.get(),

            listener_src: None,

            selected_adapter: None,
        };

        let self_opt_rc = Some(Rc::new(RefCell::new(ctrl)));
        let ctrl_rc = self_opt_rc.clone().unwrap();
        ctrl_rc.borrow_mut().self_opt_rc = self_opt_rc;

        ctrl_rc.borrow().window.set_application(app);
        let width = ctrl_rc.borrow().window.get_preferred_width();
        let height = ctrl_rc.borrow().window.get_preferred_height();
        ctrl_rc.borrow().window.set_default_size(width.1, height.1);

        ctrl_rc
            .borrow()
            .adapters_combobox
            .set_model(Some(&ctrl_rc.borrow().adapters_store));
        ctrl_rc.borrow().adapters_combobox.set_active(-1);
        let text_cell = gtk::CellRendererText::new();
        ctrl_rc
            .borrow()
            .adapters_combobox
            .pack_start(&text_cell, true);
        ctrl_rc
            .borrow()
            .adapters_combobox
            .add_attribute(&text_cell, "text", 0);

        ctrl_rc.borrow().register_actions();
        ctrl_rc.borrow().register_app_menu();
        ctrl_rc.borrow().register_callbacks();

        ctrl_rc
    }

    pub fn show_all(&self) {
        self.window.show();
        self.window.activate();
    }

    pub fn quit(&mut self) {
        self.remove_listener();
        self.window.destroy();
        self.app.quit();
    }

    fn register_listener(&mut self, listener_id: glib::SourceId) {
        if self.listener_src.is_some() {
            panic!("trying to assign new listener while old listener is active");
        }

        self.listener_src = Some(listener_id);
    }

    fn remove_listener(&mut self) {
        if let Some(source_id) = self.listener_src.take() {
            glib::source_remove(source_id);
        }
    }

    fn register_actions(&self) {
        let quit_action = gio::SimpleAction::new("quit", None);
        let self_rc = self.self_opt_rc.clone().unwrap();
        quit_action.connect_activate(move |_, _| {
            self_rc.borrow_mut().quit();
        });
        self.app.add_action(&quit_action);
    }

    fn register_app_menu(&self) {
        use gio::MenuExt;

        let menu = gio::Menu::new();
        menu.append("Quit", "app.quit");

        self.app.set_app_menu(&menu);
    }

    fn register_callbacks(&self) {
        let self_rc = self.self_opt_rc.clone().unwrap();
        self.window
            .connect_delete_event(clone!(self_rc => move |_, _| {
            self_rc.borrow_mut().quit();
            gtk::Inhibit(false)
        }));

        self.refresh_button
            .connect_clicked(clone!(self_rc => move |button| {
            button.set_sensitive(false);
            let (tx, rx) = channel::<ContextMessage>();

            if self_rc.borrow().listener_src.is_some() {
                panic!("Double listener");
            }

            self_rc.borrow_mut().register_listener(gtk::timeout_add(100, clone!(self_rc => move || {
                let mut stop_listener: bool = false;
                for message in rx.try_iter() {
                    match message {
                        ContextMessage::StartRefreshAdapters => {
                            self_rc.borrow().adapters_combobox.set_sensitive(false);
                            self_rc.borrow().scan_button.set_sensitive(false);
                        },
                        ContextMessage::StopRefreshAdapters { adapters } => {
                            stop_listener = true;

                            self_rc.borrow_mut().selected_adapter = None;
                            self_rc.borrow().adapters_combobox.set_active(-1);
                            self_rc.borrow().adapters_store.clear();
                            for adapter in adapters {
                                self_rc.borrow().adapters_store
                                    .insert_with_values(
                                        None,
                                        &[0],
                                        &[&adapter.name],
                                    );
                            }

                            self_rc.borrow().refresh_button.set_sensitive(true);
                            self_rc.borrow().adapters_combobox.set_sensitive(true);
                        },
                    }
                }

                if stop_listener {
                    self_rc.borrow_mut().remove_listener();
                }

                glib::Continue(!stop_listener)
            })));

            thread::spawn(move || {
                tx.send(ContextMessage::StartRefreshAdapters).unwrap();
                tx.send(ContextMessage::StopRefreshAdapters {
                    adapters: ble::adapters().unwrap(),
                }).unwrap();
            });
        }));

        self.adapters_combobox
            .connect_changed(clone!(self_rc => move |combobox| {
                let active = combobox.get_active_iter();
                if let Some(iter) = active {
                    let adapter_name = self_rc.borrow().adapters_store.get_value(&iter, 0).get::<String>().unwrap();
                    let mut found = None;
                    for adapter in ble::adapters().unwrap() {
                        if adapter.name == adapter_name {
                            found = Some(adapter.clone());
                            break;
                        }
                    }
                    let is_found = found.is_some();
                    self_rc.borrow_mut().selected_adapter = found;
                    self_rc.borrow().scan_button.set_sensitive(is_found);
                }
            }));
    }
}
