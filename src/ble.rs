// use std::thread;
// use std::time::Duration;

use failure::{Error, ResultExt};

// use regex::Regex;
// use rumble::api::{BDAddr, Central, CentralEvent, Characteristic, Peripheral, ValueNotification,
                //   UUID};
use rumble::bluez::adapter::Adapter;
use rumble::bluez::manager::Manager;

/*
#[derive(Debug)]
enum SensorContact {
    NotSupported,
    NotDetected,
    Detected,
}

#[derive(Debug)]
struct HeartRateMeasurement {
    heart_rate_value: u16,
    sensor_contact: SensorContact,
    energy_expended: Option<u16>, // kilo Joules
    rr_interval: Vec<Duration>,
}
*/

/*
fn parse_bdaddr(s: &str) -> Option<BDAddr> {
    use std::u8;

    let bdaddr_regex = Regex::new(r"\A([0-9A-Fa-f]{2}):([0-9A-Fa-f]{2}):([0-9A-Fa-f]{2}):([0-9A-Fa-f]{2}):([0-9A-Fa-f]{2}):([0-9A-Fa-f]{2})\z").unwrap();
    if !bdaddr_regex.is_match(s) {
        return None;
    }
    let mut result = BDAddr { address: [0; 6] };
    if let Some(captures) = bdaddr_regex.captures(s) {
        for (i, c) in captures.iter().skip(1).enumerate() {
            if let Some(cc) = c {
                result.address[5 - i] = u8::from_str_radix(cc.as_str(), 16).unwrap();
            }
        }
    }
    Some(result)
}
*/

/*
fn connect_to_adapter(adapter: Option<String>) -> Result<ConnectedAdapter, Error> {
    println!("Finding BLE adapter ...");
    let manager = Manager::new().context("Creating BLE manager failed.")?;

    let adapters = manager.adapters().context("Getting BLE adapters failed.")?;
    if adapters.len() == 0 {
        bail!("Any BLE adapter is not found.")
    }

    let mut selected_adapter: Option<Adapter> = None;
    if adapter.is_none() {
        selected_adapter = Some(adapters[0].clone());
    } else {
        let adapter_name = adapter.unwrap();
        let adapter_addr: Option<BDAddr> = parse_bdaddr(&adapter_name);
        for a in adapters {
            if let Some(addr) = adapter_addr {
                if a.addr == addr {
                    selected_adapter = Some(a.clone());
                    break;
                }
            } else {
                if a.name == adapter_name {
                    selected_adapter = Some(a.clone());
                    break;
                }
            }
        }
    }

    if let Some(adapter) = selected_adapter {
        println!("Selected adapter: {} / {}", adapter.name, adapter.addr);
        Ok(manager.connect(&adapter).context(format!(
            "Connecting to adapter {}/{} failed.",
            adapter.name, adapter.addr
        ))?)
    } else {
        bail!("Specified adapter is not found.")
    }
}
*/

pub fn adapters() -> Result<Vec<Adapter>, Error> {
    let manager = Manager::new().context("Creating BLE manager failed.")?;
    Ok(manager.adapters().context("Getting adapters list failed.")?)
}

/*
pub fn scan(adapter: Option<String>, timeout: u64) -> Result<(), Error> {
    let connected_adapter = connect_to_adapter(adapter)?;
    connected_adapter.on_event(Box::new(|event: CentralEvent| match event {
        CentralEvent::DeviceDiscovered(addr) => {
            println!("Found new device {}", addr);
        }
        CentralEvent::DeviceLost(addr) => {
            println!("Lost device {}", addr);
        }
        _ => {}
    }));
    connected_adapter
        .start_scan()
        .context("Starting scanning failed.")?;
    thread::sleep(Duration::from_secs(timeout));
    connected_adapter
        .stop_scan()
        .context("Stopping scanning failed.")?;

    Ok(())
}
*/

/*
pub fn connect(adapter: Option<String>, address: String) -> Result<(), Error> {
    let connected_adapter = connect_to_adapter(adapter)?;
    let peripheral_addr = parse_bdaddr(&address).ok_or(format_err!(
        "Can't parse the monitor address (expected format: XX:XX:XX:XX:XX:XX)."
    ))?;

    connected_adapter.start_scan()?;
    thread::sleep(Duration::from_millis(1000));
    let peripheral = connected_adapter
        .peripheral(peripheral_addr)
        .ok_or(format_err!(
            "Can't find the monitor with specified address '{}'.",
            peripheral_addr
        ))?;

    peripheral
        .connect()
        .context("Connecting to the monitor failed.")?;

    let props = peripheral.properties();
    if let Some(local_name) = props.local_name {
        println!(
            "The connection has been established to '{}' ({}).",
            local_name, peripheral_addr
        );
    } else {
        println!(
            "The connection has been established to '{}'.",
            props.address
        );
    }

    let chars = peripheral
        .discover_characteristics()
        .context("Discovering the monitor characteristics failed.")?;

    let battery_uuid = UUID::B16(0x2A19);
    let heart_rate_uuid = UUID::B16(0x2A37);

    let mut battery_char: Option<Characteristic> = None;
    let mut heart_rate_char: Option<Characteristic> = None;

    for ch in chars {
        let ch_uuid = ch.uuid;
        if ch_uuid == battery_uuid {
            battery_char = Some(ch);
            continue;
        }
        if ch_uuid == heart_rate_uuid {
            heart_rate_char = Some(ch);
            continue;
        }
    }

    peripheral.on_notification(Box::new(|val: ValueNotification| {
        if val.handle == 0x0012 {
            let data = val.value;
            if data.len() == 0 {
                // skip empty data
                return;
            }
            let mut offset = 1;
            let format = data[0];

            let mut meas = HeartRateMeasurement {
                heart_rate_value: 1,
                sensor_contact: SensorContact::NotSupported,
                energy_expended: None,
                rr_interval: Vec::new(),
            };

            if (format & 0x01) == 0 {
                // read u8
                meas.heart_rate_value = data[offset] as u16;
                offset += 1;
            } else {
                // read u16
                meas.heart_rate_value = (data[offset] as u16) + 256 * (data[offset + 1] as u16);
                offset += 2;
            }

            if (format & (1 << 2)) == 0 {
                meas.sensor_contact = SensorContact::NotSupported;
            } else {
                if (format & (1 << 1)) == 0 {
                    meas.sensor_contact = SensorContact::NotDetected;
                } else {
                    meas.sensor_contact = SensorContact::Detected;
                }
            }

            if (format & (1 << 3)) != 0 {
                meas.energy_expended =
                    Some((data[offset] as u16) + 256 * (data[offset + 1] as u16));
                offset += 2;
            }

            if (format & (1 << 4)) != 0 {
                loop {
                    if offset >= data.len() {
                        break;
                    }

                    let rr_value: u16 = (data[offset] as u16) + 256 * (data[offset + 1] as u16);
                    offset += 2;
                    meas.rr_interval
                        .push(Duration::from_millis(rr_value as u64));
                }
            }

            println!("Measurement: {:?}", meas);
        }
    }));

    if let Some(ref battery) = battery_char {
        let data = peripheral
            .read_by_type(battery, battery_uuid)
            .context("Reading battery charge level failed.")?;
        if data.len() < 5 {
            bail!("Can't read battery charge level from the monitor.");
        }
        println!("Battery charge level: {}%.", data[4]);
    }

    if let Some(ref heart_rate) = heart_rate_char {
        peripheral
            .subscribe(heart_rate)
            .context("Subscribing to heart rate measurement characteristic failed.")?;
    }

    loop {
        thread::sleep(Duration::from_millis(10));
    }

    // peripheral
    //     .disconnect()
    //     .context("Disconnecting from the monitor failed.")?;

    // connected_adapter.stop_scan()?;
}
*/
