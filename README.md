# HeartRate

GNOME application for monitoring heart rate with BLE probe.
It was tested with 'Polar H7' only.

## Building

* Install [rust](https://www.rust-lang.org).
* Install [cargo](https://doc.rust-lang.org/cargo/).
* Run: `cargo build --release`.
* Run built binary `target/release/.../heartrate`.
